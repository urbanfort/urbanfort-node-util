// # Tools
//
// ## Summary
// Tools are predimonantly differentiated from other handlers in
// that while they could conceivably work with callbacks, in most
// cases they don't need to.
//
// Use Tools to "drop off" some data for processing while your method
// continues execution elsewhere.
var moment = require("moment"),
    debug = require("debug")("uf"),
    fs = require("fs"),
    path = require("path"),
    child_process = require("child_process"),
    debug = require("debug")("uf"),

    tools = {

        // Create a daily total compatible stat value.
        dailyStat:
            function (reftable, refid, tag, total) {
                "use strict";
                tools.app.db.Stat.findOrCreate(
                    {
                        tdate: tools.app.db.Stat.today(),
                        reftable: reftable,
                        refid: refid,
                        tag: tag
                    }
                ).success(
                    function (stat, isCreated) {
                        if (isCreated) {
                            stat.total = total;
                        } else {
                            stat.total += total;
                        }
                        stat.save();
                    }
                ).error(
                    function (errors) {
                        debug("Error making daily stat", errors);
                    }
                );
            },

        systemStatus:
            function () {
                // TODO: System status should be fetched from a cache graph
                // that is updated by the various components in the system.
                return {};
            },

        buildExtStore:
            function (forStoreString) {
                var output = "";

                output += "Ext.define(\n";
                output += "\"Shellac.store." + forStoreString + "\",\n";
                output += JSON.stringify(
                    {
                        extend: "Ext.data.Store",
                        model: "Shellac.model." + forStoreString,
                        autoLoad: true,
                        proxy: {
                            type: "ajax",
                            url: "/" + forStoreString.toLowerCase(),
                            reader: {type: "json", root: "data"}
                        }
                    }
                );
                output += ");";
                return output;
            },
    
        // ## Produce an Ext.data.Model Def.
        buildExtModel:
            function (forModelString) {
                var model = tools.app.db[forModelString],
                    fieldDefs = tools.clientCompatibleFieldDefs(model),
                    output = "";

                output += "Ext.define(\"Shellac.model." + forModelString + "\",\n";
                output += JSON.stringify(
                    {
                        "extend": "Ext.data.Model",
                        "fields": fieldDefs
                    }
                ) + "\n";
                output += (");");

                return output;
            },

        // ## Build a List of Fields
        // This builds a field list that is compatible with Ext.
        // Sadly our naming convention probably won't support
        // additional platforms but goddamn if I'm gonna write all this
        // boilerplate by hand.
        clientCompatibleFieldDefs:
            function (model) {
                var fieldDefs = [{"name": "id", "type": "int"}],
                    blacklist = ["id", "openId", "google_access_token", "google_refresh_token"],
                    fieldKey,
                    field,
                    fieldType;
                
                for (fieldKey in model.rawAttributes) {
                    if (model.rawAttributes.hasOwnProperty(fieldKey)) {
                        if (blacklist.indexOf(fieldKey) < 0) {
                            field = model.rawAttributes[fieldKey];
                            fieldDefs.push(
                                {
                                    "name": fieldKey,
                                    "type": tools.sequelizeToExtFieldType(field)
                                }
                            );
                        }
                    }
                }

                return fieldDefs;
            },

        // ## Convert Field Types
        // This converts between Sequelize and Ext.data.Model
        // field formats. If a field type is not represented it
        // (the type) will not be included in the  output and Ext
        // will infer a type of "string" for safety.
        sequelizeToExtFieldType:
            function (field) {
                var fieldType,
                    fieldMatch = {
                        "INTEGER": "int",
                        "STRING": "string",
                        "VARCHAR": "string",
                        "DATETIME": "date"
                    };

                if (field.hasOwnProperty("_typeName")) {
                    fieldType = fieldMatch[field._typeName];
                } else if (field.hasOwnProperty("type") &&
                    typeof(field.type) === "string") {
                    fieldType = fieldMatch[field.type];
                }

                return fieldType;
            },

        whitelist:
            function (url) {
                "use strict";
                return true;
            },

        // TODO: Find any .less files in project and parse them.
        less:
            function (basepath) {
                "use strict";
                child_process.exec(
                    "lessc " + basepath + "public/stylesheets/style.less > " + basepath + "public/stylesheets/style.css",
                    function (err, stdout, stderr) {
                        if (err) {
                            debug("err");
                        }
                        debug("Baked final style.css");
                    }
                );
            },

        globify:
            function () {
                "use strict";
                tools.app.use(
                    function (req, res, next) {
                        var localRender = res.render,
                            i;
                        res.render = function (view, locals, callback) {
                            if ('function' == typeof locals) {
                                callback = locals;
                                locals = undefined;
                            }
                            if (!locals) {
                                locals = {};
                            }
                            locals.title = tools.app.pkg.name;
                            locals.appVersion = tools.app.pkg.version + " " + tools.app.config.version;
                            locals.contributors = tools.app.pkg.contributors;

                            req.session.breadcrumbs = [];
                            req.session.breadcrumbs.push({url: "#", label: "Bob"});
                            req.session.breadcrumbs.push({url: "#", label: "Jones"});
                            req.session.breadcrumbs.push({url: "#", label: "Was Here"});

                            if (tools.app.hasOwnProperty("bag")) {
                                locals.bag = tools.app.bag;
                            }

                            if (tools.app.hasOwnProperty("luggage")) {
                                locals.luggage = tools.app.luggage;
                            } else {
                                debug("No luggage found.");
                            }

                            locals.req = req;
                            locals.moment = moment;

                            if (tools.app.hasOwnProperty("bag")) {
                                locals.bag = tools.app.bag;
                            }

                            localRender.call(res, view, locals, callback);
                        };
                        next();
                    }
                );
            },

        passify:
            function () {
                "use strict";
                tools.app.all(
                    "/*",
                    function (req, res, next) {
                        var bypass = false,
                            usplit = req.url.split("/");

                        if (usplit.length > 1) {
                            if (usplit[1] === "api") {
                                req.headers["X-Requested-With"] = "Rest";
                                req.url = req.originalUrl.replace(/\/api/, "");
                                next();
                                return;
                            }
                        }

                        if (req.get("X-Requested-With") === "XMLHttpRequest") {
                            req.headers["X-Requested-With"] = "Rest";
                        }

                        if (req.query.hasOwnProperty("format")) {
                            tools.app.format = req.query.format;
                        }

                        if (req.params.hasOwnProperty("format")) {
                            tools.app.format = req.params.format;
                        }

                        if (req.body.hasOwnProperty("format")) {
                            tools.app.format = req.body.format;
                        }

                        if (req.url === "/" || tools.whitelist(req.url)) {
                            bypass = true;
                        }

                        if (req.hasOwnProperty("session") &&
                            req.session.hasOwnProperty("UserId") === true) {
                            bypass = true;
                        }

                        if (req.hasOwnProperty("session") === true &&
                            req.session.hasOwnProperty("cookie") === false) {
                            bypass = false;
                        }

                        if (bypass) {
                            if (req.hasOwnProperty("session")) {
                                req.session.cookie.expires = moment().add("days", 30).toDate();
                                req.session.cookie.maxAge = 2592000000;
                            }
                            next();
                        } else {
                            res.redirect("/");
                        }
                    }
                );
            },

        routify:
            function (routes) {
                "use strict";
                var route;


                for (route in routes) {
                    debug("Setting up route: ", route);
                    routes[route].app = tools.app;
                    var path = route === 'index' ? '/' : '/' + route;
                    tools.app.use(path, routes[route]);

                    if (routes[route].hasOwnProperty("bind")) {
                        routes[route].bind();
                    }
                }
            }

};

module.exports = function (_app) {
    var undef;
    if (_app === undef) {
        throw new Error('You cannot build tools without an app.\n\tUse: tools = require(pathspec)(app)\n');
    }
    tools.app = _app;
    return tools;
};
