var path = require("path");

var responses = {

    error:
        function (res, data) {
            res.status(500);
            res.send(JSON.stringify(data));
        },

    errorMissing:
        function (res, data) {
            res.status(404);
            res.send(JSON.stringify(data));
        },

    errorUnauthorized:
        function (res, data) {
            res.status(401);
            res.send(JSON.stringify(data));
        },

    index:
        function (res, data) {
            if (!(data instanceof Array)) {
                data = [data];
            }
            res.send(JSON.stringify({"data": data}));
        },

    detail:
        function (res, data) {
            res.send(JSON.stringify({"data": data}));
        },

    renderForm:
        function (req, res, graph) {
            if (req.get("X-Requested-With") === "XMLHttpRequest") {
                res.render(["form", graph.forminclude].join(path.sep), graph);
            } else {
                res.render("fullform", graph);
            }
        },

    renderDetail: function (req, res, graph) {
            if (req.headers["X-Requested-With"] === "XMLHttpRequest" ||
                req.headers["X-Requested-With"] === "Rest") {
                responses.detail(res, graph.obj[_.first(_.keys(graph.obj))]);
            } else {
                res.render(graph.include, graph.obj);
            }
        }

};

module.exports = function (_app) {
    responses.app = _app;
    return responses;
};
